// Code challenge for Junior Frontend Engineer Position
// RUN: node metroCodeChallenge.js
// STOP: Ctrl + C

class Metro {
    constructor(railways, maxPassengersInTrain) {
        this.railways = railways;
        this.maxPassengersInTrain = maxPassengersInTrain;
        this.trains = [];    
    }

    // adds a train to a railway
    addTrain(train) {
        if (train instanceof Train) {
            this.trains.push(train);
            this.sortTrainsByNumberOfPassengers(this.trains);
        }  
    }

    // sorts trains by Number of passengers so that trains with more passengers will be able to move first
    sortTrainsByNumberOfPassengers(trains) {
        trains.sort(function (a, b) {
            return b.passengers - a.passengers;
        });
    }

    // moves all trains one step forward
    run() {
        let busyStations = [];
        for (let train of this.trains) {
            if (busyStations.indexOf(train.railwayStations[train.nextStationIndex]) == -1) {
                train.move();
            } else {
                console.log(`\nRailway ${train.railwayNumber}: Train is waiting. Station ${train.railwayStations[train.nextStationIndex]} is busy.`);
            }     
            console.log(`\nStation ${train.railwayStations[train.stationIndex]}:`);
            console.log(train);
            busyStations.push(train.railwayStations[train.stationIndex]);
            // console.log('busy stations:', busyStations);
        }
        console.log('\n--------------------------------------------\n');
    }
}

class Train {
    constructor(railwayNumber, railwayStations, stationIndex, direction, passengers) {
        this.railwayNumber = railwayNumber,
        this.railwayStations = railwayStations;
        this.stationIndex = stationIndex;
        this.direction = direction;   // -1 direction from the end of railway or 1 direction start of the railway
        this.passengers = passengers;
    }

    // moves train one step forward
    move() {
        this.stationIndex = this.nextStationIndex;
    }

    // return index of the next station
    get nextStationIndex() {
        // when current station is not terminus
        if (this.stationIndex == 0) {
            this.direction = 1;
            return this.stationIndex + 1;
        } else if (this.stationIndex == this.railwayStations.length - 1) {
            this.direction = -1;
            return this.stationIndex - 1;
        }
        return this.stationIndex + this.direction;
    }
}


 // test movement of all trains in one time interval
function testMove() {
    const metro = createDummyMetro();

    const train = new Train('1', metro.railways['1'], 0, 1, 25);
    metro.addTrain(train);
    train.move();

    if (train.stationIndex === 1 && train.direction === 1) {
        console.log('move test passed');
    } else {
        console.log('move test failed');
    }
}

 // test if upon reaching a terminus station trains change direction
function testChangeDirection() {
    const metro = createDummyMetro();

    const train1 = new Train('1', metro.railways['1'], 0, -1, 25);
    metro.addTrain(train1);
    train1.move();

    const train2 = new Train('2', metro.railways['2'], 4, 1, 25);
    metro.addTrain(train2);
    train2.move();

    if ((train1.stationIndex === 1 && train1.direction === 1) && 
        (train2.stationIndex === 3 && train2.direction === -1)) {
        console.log('change direction test passed');
    } else {
        console.log('change direction test failed');
    }
}

// test if two trains going to meet on the station, the one with fewer passengers should wait
function testJunction() {
    const metro = createDummyMetro();

    const train1 = new Train('1', metro.railways['1'], 1, 1, 50);
    metro.addTrain(train1);

    const train2 = new Train('2', metro.railways['2'], 1, 1, 49);
    metro.addTrain(train2);

    metro.run();

    if (train1.stationIndex === 2 && train2.stationIndex === 1) {
        console.log('junction test passed');
    } else {
        console.log('junction test failed');
    }
}

// Functions to create dummy data and trains

function getRandomDirection() {
    return Math.random() < 0.5 ? -1 : 1;
}

function getRandomInt(lowerLimit, upperLimit) {
    return Math.floor(Math.random() * (upperLimit - lowerLimit + 1)) + lowerLimit;
}

function addRandomTrains(metro) {
    for (let railway in metro.railways) {
        const train = new Train(
            railway,
            metro.railways[railway],
            getRandomInt(0, metro.railways[railway].length - 1),
            getRandomDirection(),
            getRandomInt(0, metro.maxPassengersInTrain) // check if it is not equal N of passengers in existing trains
        );
        metro.addTrain(train);
    }

    return metro;
}

function createDummyMetro() {
    const railways = {1: [0, 1, 2, 3, 4, 5],
        2: [6, 7, 2, 8, 9],
        3: [10, 11, 7, 2, 12, 13, 14]};

    const m = new Metro(railways, 100);
    return m;
}

function runExample() {
    const metro = createDummyMetro();
    addRandomTrains(metro);
    setInterval(() => metro.run(), 1000);
}

console.log('*********** UNIT TESTS ************\n');
testMove();
testChangeDirection();
testJunction();
console.log('\n*********** RUN EXAMPLE ************\n');
runExample();